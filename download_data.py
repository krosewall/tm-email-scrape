import imaplib
import email
import re
import sys
import datetime

#Green Monday.mp3 has been downloaded by someone with the ip address: 37.142.55.110

u = 'info@twinmusicom.org'
p = '5P@Rkyn@!1'
fn = 'output/data.csv'
f = open(fn, "w")
f2 = open('output/error.txt',"w")
f.write("Date,Time,Name,IP\n")

m = imaplib.IMAP4_SSL('mail.privateemail.com')
m.login(u,p)
m.select("Downloads")
typ, data = m.search(None, 'ALL')
for num in data[0].split():
	typ, data = m.fetch(num,'(RFC822)')
	message = email.message_from_string(data[0][1])
	match = re.match("(.*\.mp3) has been downloaded by someone with the ip address: (.*)\r\n", message.get_payload(decode=True))
	local_date = datetime.datetime.fromtimestamp(email.utils.mktime_tz(email.utils.parsedate_tz(message['Date'])))
	print(local_date.strftime("%m/%d/%Y %H:%M:%S"))
	try:
		f.write(local_date.strftime("%m/%d/%Y") + "," + local_date.strftime("%I:%M:%S %P") + "," + match.groups(0)[0] + "," + match.groups(0)[1] + "\n")
	except :
		f2.write(message.get_payload(decode=True))

m.close()
m.logout()
f.close()