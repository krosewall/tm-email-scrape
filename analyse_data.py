import sys
import csv
import collections
import datetime

columns = collections.defaultdict(list)
today = datetime.date.today()
fn = 'output/data.csv'
outn = 'output/output-' + today.strftime("%m-%d-%Y") + '.txt'
out = open(outn,'w')
csv_file = []

with open(fn) as f:
	reader = csv.DictReader(f)
	for row in reader:
		csv_file.append(row)
		for (k,v) in row.items():
			columns[k].append(v)
			
count_date = collections.Counter(columns['Date'])
count_time = collections.Counter(columns['Time'])
count_name = collections.Counter(columns['Name'])
count_ip = collections.Counter(columns['IP'])

out.write('Analysis of downloaded files | Twin Musicom\n')
out.write('Date: ' + today.strftime("%m/%d/%Y") + '\n\n')
out.write('Frequency of files\n')
out.write('====================================================\n')

for song,count in count_name.most_common(len(columns['Name'])):
	line = song
	for i in range(50-len(song)):
		line += '.'
	line += str(count) + '\n'
	out.write(line)

out.write('\n\n')
out.write('Dates with most downloads\n')
out.write('====================================================\n')
for d,count in count_date.most_common(len(columns['Date'])):
	line = d
	for i in range(50-len(d)):
		line += '.'
	line += str(count) + '\n'
	out.write(line)
	
out.write('\n\n')
out.write('IPs that downloaded the most\n')
out.write('====================================================\n')
for ip,count in count_ip.most_common(len(columns['IP'])):
	line = ip
	for i in range(50-len(ip)):
		line += '.'
	line += str(count) + '\n'
	out.write(line)
	
out.close()